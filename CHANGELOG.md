# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [2.0.1](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/compare/v2.0.0...v2.0.1) (2022-02-04)


### Bug Fixes

* Removed empty lines ([6cd8509](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/6cd85095d5ec6f25b77dc52c2b5676c163a14e41))

## [2.0.0](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/compare/v1.1.0...v2.0.0) (2022-02-04)


### ⚠ BREAKING CHANGES

* **frontend:** Changed parameters

### Features

* **frontend:** Optimized Feature 1 -> 1a ([2847738](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/28477384dfc334eda67629e0c5c52a4a5d8f3c15))


### Bug Fixes

* **backend:** Fix typo in Feature 2 ([f12593d](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/f12593d259cbca8e82d003d66ffedb99bd2c244f))

## [1.1.0](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/compare/v1.0.0...v1.1.0) (2022-02-04)


### Features

* **backend:** Add Demo-Feature 2 ([483872b](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/483872ba62407de3313d733b40645c116e8b1c6c))
* **frontend:** Add Demo-Feature 1 ([0ad9cde](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/0ad9cde9f6ce2dce0c197ae18d6e83935df06055))

## 1.0.0 (2022-02-04)


### Features

* Initial commit ([185af69](https://gitlab.com/philipp.doblhofer/automatic-changelog-demo/commit/185af697b60c4cd08a9a9c310dee99e63de1c035))
